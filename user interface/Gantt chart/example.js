/* --- src/example-common.js --- */
var Example = {};

function extend(child, parent) {
    var F = function () {
    };
    F.prototype = parent.prototype;
    child.prototype = new F();
    child.prototype.constructor = child;
    child.superclass = parent.prototype;
}


/* --- src/example-paintPanel.js --- */
/**
 * Paint panel.
 */

data = [];
idtfs = [];


Example.PaintPanel = function (containerId) {
    this.containerId = containerId;
};

Example.PaintPanel.prototype = {

    init: function () {
        this._initMarkup(this.containerId);
    },

    _initMarkup: function (containerId) {
        var container = $('#' + containerId);

        var self = this;

	container.append('<div id="content"></div>');
	$('#content').append('<div id="diagram"></div>');
	$('#content').append('<div id="description"></div>');
	

	content = document.getElementById("content");
	diagram = document.getElementById("diagram");
	description = document.getElementById("description");

	content.style.cssText = "\
		display: inline-block; \
    		margin-bottom: 30px; \
    		overflow-x: auto; \
    		overflow-y: hidden; \
		";

	diagram.style.cssText = "\
		display: inline-block; \
    		margin-bottom: 30px; \
    		overflow-x: auto; \
    		overflow-y: hidden; \
		";

	description.style.cssText = "\
		width: 100%;\
    		margin-top: 40px;\
		";		
	
	findTasks();
	
	function findTasks() {
		console.log("inFind");
		var dm_task, project_task;
		SCWeb.core.Server.resolveScAddr(['project_task'], function (keynodes) {
			project_task = keynodes['project_task'];
			window.sctpClient.iterate_elements(SctpIteratorType.SCTP_ITERATOR_3F_A_A, [
 				project_task,
 				sc_type_arc_pos_const_perm,
 				sc_type_node | sc_type_const]).
			done(function(identifiers){
				identifiers.forEach((elem) => {
					findDeadlines(elem[2]);
				})
				setTimeout(completeComponent, 3000);
			});
		});
	}

	function completeComponent() {
		checkData();
        	var minDate = findMinDate();
        	var maxDate = findMaxDate();
        	var timeInterval = getTimeInterval(minDate, maxDate);
        	buildDiagram(timeInterval, diagram, description);	
	}

	function findDeadlines(task) {
		console.log('searching deadlines');
		var nrel_deadlines;
		SCWeb.core.Server.resolveScAddr(['nrel_deadlines'], function (keynodes) {
			nrel_deadlines = keynodes['nrel_deadlines'];
			window.sctpClient.iterate_elements(SctpIteratorType.SCTP_ITERATOR_5F_A_A_A_F, [
	 			task,
	 			sc_type_arc_common | sc_type_const,
				sc_type_node | sc_type_const,
	 			sc_type_arc_pos_const_perm,
	 			nrel_deadlines]).
			done(function(identifiers){
				window.scHelper.getIdentifier(task, scKeynodes.nrel_main_idtf).done(function(content){
					 idtfs.push(content);
				});
				var object = {
					name: '',
					aims: [
				   		{
				       		caption: 'a',
				       		from: '',
				       		to: ''
				   		}
			      		]
				}
				window.scHelper.getIdentifier(task, scKeynodes.lang_ru).done(function(content){
				 	object.name = content;
					findFromNode(identifiers[0][2], object);
					findByNode(identifiers[0][2], object);
					addTaskToData(object);
				 });

			});
		});
	}

	function addTaskToData(task) {
		data.push(task);	
	}

	function findFromNode(deadlines, object) {
		var rrel_from;
		SCWeb.core.Server.resolveScAddr(['rrel_from'], function (keynodes) {
			rrel_from = keynodes['rrel_from'];
			window.sctpClient.iterate_elements(SctpIteratorType.SCTP_ITERATOR_5F_A_A_A_F, [
	 			deadlines,
	 			sc_type_arc_pos_const_perm,
				sc_type_node | sc_type_const,
	 			sc_type_arc_pos_const_perm,
	 			rrel_from]).
			done(function(identifiers){
				findTimeValue(identifiers[0][2], 0, object);	 		
			});
		});
	}

	function findByNode(deadlines, object) {
		var rrel_by;
		SCWeb.core.Server.resolveScAddr(['rrel_by'], function (keynodes) {
			rrel_by = keynodes['rrel_by'];
			window.sctpClient.iterate_elements(SctpIteratorType.SCTP_ITERATOR_5F_A_A_A_F, [
	 			deadlines,
	 			sc_type_arc_pos_const_perm,
				sc_type_node | sc_type_const,
	 			sc_type_arc_pos_const_perm,
	 			rrel_by]).
			done(function(identifiers){
				//console.log('By value found');
				findTimeValue(identifiers[0][2], 1, object);	 		
			});
		});
	}

	function findTimeValue(node, type, object) {
		var translation;
		SCWeb.core.Server.resolveScAddr(['nrel_translation_in_the_Gregorian_calender'], function (keynodes) {
			translation = keynodes['nrel_translation_in_the_Gregorian_calender'];
			window.sctpClient.iterate_elements(SctpIteratorType.SCTP_ITERATOR_5F_A_A_A_F, [
	 			node,
	 			sc_type_arc_common | sc_type_const,
				sc_type_node | sc_type_const,
	 			sc_type_arc_pos_const_perm,
	 			translation]).
			done(function(identifiers){
				findDate(identifiers[0][2], type, object);	 		
			});
		});
	}

	function findDate(node, type, object) {
		var date = [];
		var day, month, year;
		SCWeb.core.Server.resolveScAddr(['rrel_day', 'rrel_month', 'rrel_year'], function (keynodes) {
			day = keynodes['rrel_day'];
			month = keynodes['rrel_month'];
			year = keynodes['rrel_year'];
			window.sctpClient.iterate_elements(SctpIteratorType.SCTP_ITERATOR_5F_A_A_A_F, [
	 			node,
	 			sc_type_arc_pos_const_perm,
				sc_type_link,
	 			sc_type_arc_pos_const_perm,
	 			year]).
			done(function(identifiers){
				window.sctpClient.get_link_content(identifiers[0][2],'string').done(function(content){
					date.push(content);
				});
			});

			window.sctpClient.iterate_elements(SctpIteratorType.SCTP_ITERATOR_5F_A_A_A_F, [
	 			node,
	 			sc_type_arc_pos_const_perm,
				sc_type_link,
	 			sc_type_arc_pos_const_perm,
	 			month]).
			done(function(identifiers){
				window.sctpClient.get_link_content(identifiers[0][2],'string').done(function(content){
					date.push(content);
				});		
			});
		
			window.sctpClient.iterate_elements(SctpIteratorType.SCTP_ITERATOR_5F_A_A_A_F, [
	 			node,
	 			sc_type_arc_pos_const_perm,
				sc_type_link,
	 			sc_type_arc_pos_const_perm,
	 			day]).
			done(function(identifiers){
				window.sctpClient.get_link_content(identifiers[0][2],'string').done(function(content){
					date.push(content);	
					if(type == 0) {
						if(date[1].length != 2)
							date[1] = '0' + date[1];
						if(date[2].length != 2)
							date[2] = '0' + date[1];
						object.aims[0].from = date[0] + '.' + date[1] + '.' + date[2];
					}
					else {
						if(date[1].length != 2)
							date[1] = '0' + date[1];
						if(date[2].length != 2)
							date[2] = '0' + date[1];
						object.aims[0].to = date[0] + '.' + date[1] + '.' + date[2];		
					}
				});		
			});
		});
		
	}

	function checkData() {
            data.forEach((elem) => {
                if(!elem.name || (elem.aims.length == 0)) {
                    alert('The data is wrong. Check it');
                    throw new Error('The data is wrong. Check it');
                }
                elem.aims.forEach((props) => {
                    if(!props.caption || !props.from || !props.to) {
                        alert('The data is wrong. Check it');
                        throw new Error('The data is wrong. Check it');
                    }
                });
            });
        }

        function findMinDate() {
            var minDate = '9999.99.99';
            var dates = [];

            data.forEach((elem) => {
                elem.aims.forEach((aim) => {dates.push(aim.from)});
            });

            dates.forEach((elem) => {
                if(elem < minDate) {
                    minDate = elem;
                }
            });

            return minDate;
        }

        function findMaxDate() {
            var maxDate = '0000.00.00';
            var dates = [];

            data.forEach((elem) => {
                elem.aims.forEach((aim) => {dates.push(aim.to)});
            });

            dates.forEach((elem) => {
                if(elem > maxDate) {
                    maxDate = elem;
                }
            });

            return maxDate;
        }

        function getTimeInterval(min, max) {

            var minYear = parseInt(min.match(/\d{2,4}/g)[0]);
            var minMonth = min.match(/\d{2,4}/g)[1] - 1;
            var minDay = parseInt(min.match(/\d+$/g));
            var maxYear = parseInt(max.match(/\d{2,4}/g)[0]);
            var maxMonth = max.match(/\d{2,4}/g)[1] - 1;
            var maxDay = parseInt(max.match(/\d+$/g));

            var startDate = new Date(minYear, minMonth, minDay);
            var endDate = new Date(maxYear, maxMonth, maxDay);
            var weeks = [];
            var week = [];

            if(startDate.getDay() != 0) {
                while(startDate.getDay() != 0) {
                    startDate.setDate(startDate.getDate() - 1);
                }
            }

            if(endDate.getDay() != 6) {
                while(endDate.getDay() != 6) {
                    endDate.setDate(endDate.getDate() + 1);
                }
            }

            var startLoop = `${startDate.getFullYear()}.${Math.floor((startDate.getMonth() + 1) / 10)}${(startDate.getMonth() + 1) % 10}.${Math.floor(startDate.getDate() / 10)}${startDate.getDate() % 10}`;
            var endLoop = `${endDate.getFullYear()}.${Math.floor((endDate.getMonth() + 1) / 10)}${(endDate.getMonth() + 1) % 10}.${Math.floor(endDate.getDate() / 10)}${endDate.getDate() % 10}`;

            while(startLoop <= endLoop) {
                if(startDate.getDay() == 6) {
                    week.push(`${startDate.getFullYear()}.${Math.floor((startDate.getMonth() + 1) / 10)}${(startDate.getMonth() + 1) % 10}.${Math.floor(startDate.getDate() / 10)}${startDate.getDate() % 10}`);
                    weeks.push(week);
                    week = [];
                    startDate.setDate(startDate.getDate() + 1);
                    startLoop = `${startDate.getFullYear()}.${Math.floor((startDate.getMonth() + 1) / 10)}${(startDate.getMonth() + 1) % 10}.${Math.floor(startDate.getDate() / 10)}${startDate.getDate() % 10}`;
                } else {
                    week.push(`${startDate.getFullYear()}.${Math.floor((startDate.getMonth() + 1) / 10)}${(startDate.getMonth() + 1) % 10}.${Math.floor(startDate.getDate() / 10)}${startDate.getDate() % 10}`);
                    startDate.setDate(startDate.getDate() + 1);
                    startLoop = `${startDate.getFullYear()}.${Math.floor((startDate.getMonth() + 1) / 10)}${(startDate.getMonth() + 1) % 10}.${Math.floor(startDate.getDate() / 10)}${startDate.getDate() % 10}`;
                }
            }

            return weeks;
        }

        function buildDiagram(tI, component, componentDescr) {
            component.innerHTML = '';
            var colors = ['#1abc9c', '#2ecc71', '#4aa3df', '#9b59b6', '#34495e', '#e67e22', '#e74c3c'];
            var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var calendar = document.createElement('div');

            //_______ Calendar
            calendar.className = 'calendar';
	    calendar.style.cssText = "\
		width: 100%; \
    		height: 40px; \
    		padding-left: 165px; \
		";

	    componentDescr.innerHTML = '';
                
            tI.forEach((week) => {
                var calendarWeek = document.createElement('div');
                calendarWeek.className = 'calendar_week';
                calendarWeek.textContent = `${months[(week[0].match(/\d{2,4}/g)[1] - 1)]} ${week[0].match(/\d+$/g)}`;
		calendarWeek.style.cssText = "\
			display: inline-block;\
    			height: 100%;\
    			width: 70px;\
    			margin-right: 2px;\
    			text-align: center;\
    			font-size: 13px;\
    			overflow: hidden;\
    			line-height: 40px;\
			";
                calendar.appendChild(calendarWeek);
            });

	    var elemIdtf = -1;

            data.forEach((elem, elemIndex) => {
                var name = document.createElement('div');
                name.textContent = elem.name;
                name.className = 'name';
		name.style.cssText = "\
			display: inline-block;\
			height: 100%;\
    			width: 200px;\
    			color: #333;\
    			text-align: center;\
    			font-size: 15px;\
    			overflow: hidden;\
    			line-height: 40px;\
			";
                var line = document.createElement('div');
                line.className = 'line';
		line.style.cssText = "\
			height: 40px;\
    			min-width: 100%;\
		    	border-bottom: 2px solid #ccc;\
			";
                line.appendChild(name);
                var aimsIntervals = [];
                var colorsMatchArray = [];
                var colorsMatch = false;

                //_______ Choose colors

                elem.aims.forEach((aI, i) => {
                    var color = colors[getRandomInteger(0, colors.length - 1)];
                    colorsMatch = colorsMatchArray.includes(color);
                    if(colorsMatch == true) {
                        while(colorsMatch == true) {
                            color = colors[getRandomInteger(0, colors.length - 1)];
                            colorsMatch = colorsMatchArray.includes(color);
                        }
                    }
                    colorsMatchArray.push(color);
                    aimsIntervals.push([aI.from, aI.to, colorsMatchArray[i], aI.caption]);
                });
		
		aimsIntervals.forEach((aIEntry, index) => {
			elemIdtf ++;
			var idtf = 'name' + elemIdtf;
                        var div = document.createElement('div');
                        var descName = document.createElement('a');
			descName.id = idtf;
			descName.style.cssText = "\
				cursor: pointer;\
				";
                        descName.textContent = `${elem.name} `;
                        var square = document.createElement('div');
                        square.className = 'square';
			square.style.cssText = "\
				display: inline-block;\
    				width: 10px;\
				height: 10px;\
				margin-right: 5px\
				";
                        square.style.backgroundColor = aIEntry[2];
			div.appendChild(square);
                        div.appendChild(descName);
                        componentDescr.appendChild(div);
                });

                //_______ Draw diagram
                tI.forEach((weeks) => {
                    var week = document.createElement('div');
                    week.className = 'week';
		    week.style.cssText = "\
			display: inline-block;\
    			height: 100%;\
    			width: 70px;\
			";
                    var separator = document.createElement('div');
                    separator.className = 'separator';
		    separator.style.cssText = "\
			display: inline-block;\
    			height: 100%;\
    			width: 2px;\
    			background-color: #ccc;\
			";

                    weeks.forEach((days) => {
                        var day = document.createElement('div');
                        day.className = 'day';
			day.style.cssText = "\
				display: inline-block;\
    				height: 100%;\
    				width: 10px;\
				";
                        var indicator = document.createElement('div');
                        indicator.className = 'indicator';
			indicator.style.cssText = "\
				height: 20px;\
    				width: 100%;\
    				top: 50%;\
    				transform: translateY(50%);\
				";

                        aimsIntervals.forEach((aIEntry) => {
                            if((days >= aIEntry[0]) && (days <= aIEntry[1])) {
                                indicator.style.backgroundColor = aIEntry[2];
                            }
                        });

                        day.appendChild(indicator);
                        week.appendChild(day);
                    });

                    line.appendChild(separator);
                    line.appendChild(week);
                });

                component.appendChild(line);
            });
	    idtfs.forEach((elem, index) => {
		$('#name' + index).click(function() {
			showTask(elem);
		});
	    });
            component.appendChild(calendar);
        }

        function getRandomInteger(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

	function showTask(taskIdtf) {
		console.log('Generated history item: ' + taskIdtf);
		var task;
		SCWeb.core.Server.resolveScAddr([taskIdtf], function (keynodes) {
			task = keynodes[taskIdtf];
			SCWeb.core.Server.resolveScAddr(["ui_menu_view_full_semantic_neighborhood"],
			function (data) {
				var cmd = data["ui_menu_view_full_semantic_neighborhood"];
				SCWeb.core.Main.doCommand(cmd,
				[task], function (result) {
					if (result.question != undefined) {
						SCWeb.ui.WindowManager.appendHistoryItem(result.question);
					}
				});
			});
		});
	}

    }
};


/* --- src/example-component.js --- */
/**
 * Example component.
 */
Example.DrawComponent = {
    ext_lang: 'GanttChart',
    formats: ['format_example_json'],
    struct_support: true,
    factory: function (sandbox) {
        return new Example.DrawWindow(sandbox);
    }
};

Example.DrawWindow = function (sandbox) {
    this.sandbox = sandbox;
    this.paintPanel = new Example.PaintPanel(this.sandbox.container);
    this.paintPanel.init();
    this.recieveData = function (data) {
        console.log("in recieve data" + data);
    };

    var scElements = {};

    function drawAllElements() {
        var dfd = new jQuery.Deferred();
       // for (var addr in scElements) {
            jQuery.each(scElements, function(j, val){
                var obj = scElements[j];
                if (!obj || obj.translated) return;
// check if object is an arc
                if (obj.data.type & sc_type_arc_pos_const_perm) {
                    var begin = obj.data.begin;
                    var end = obj.data.end;
                    // logic for component update should go here
                }

        });
        SCWeb.ui.Locker.hide();
        dfd.resolve();
        return dfd.promise();
    }

// resolve keynodes
    var self = this;
    this.needUpdate = false;
    this.requestUpdate = function () {
        var updateVisual = function () {
// check if object is an arc
            var dfd1 = drawAllElements();
            dfd1.done(function (r) {
                return;
            });


/// @todo: Don't update if there are no new elements
            window.clearTimeout(self.structTimeout);
            delete self.structTimeout;
            if (self.needUpdate)
                self.requestUpdate();
            return dfd1.promise();
        };
        self.needUpdate = true;
        if (!self.structTimeout) {
            self.needUpdate = false;
            SCWeb.ui.Locker.show();
            self.structTimeout = window.setTimeout(updateVisual, 1000);
        }
    }
    
    this.eventStructUpdate = function (added, element, arc) {
        window.sctpClient.get_arc(arc).done(function (r) {
            var addr = r[1];
            window.sctpClient.get_element_type(addr).done(function (t) {
                var type = t;
                var obj = new Object();
                obj.data = new Object();
                obj.data.type = type;
                obj.data.addr = addr;
                if (type & sc_type_arc_mask) {
                    window.sctpClient.get_arc(addr).done(function (a) {
                        obj.data.begin = a[0];
                        obj.data.end = a[1];
                        scElements[addr] = obj;
                        self.requestUpdate();
                    });
                }
            });
        });
    };
// delegate event handlers
    this.sandbox.eventDataAppend = $.proxy(this.receiveData, this);
    this.sandbox.eventStructUpdate = $.proxy(this.eventStructUpdate, this);
    this.sandbox.updateContent();
};
SCWeb.core.ComponentManager.appendComponentInitialize(Example.DrawComponent);


